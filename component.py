from string import Template
from os import path

from .vhtypes import *

p = path.abspath(__file__)
d = path.dirname(p)
tpl_dir = path.join(d, "tpl")

def binjoin_fields(*fields):
    return "".join(f.bin() for f in fields)

def get_total_len(*ports):
    return sum(p.len() for p in ports)


class Port(object):
    def __init__(self, direction, signal):
        self._dir = direction
        self._signal = signal()

    def _get_name(self):
        return self._signal._get_name()

    def _set_name(self, name):
        self._signal._set_name(name)

    def _get_signal(self):
        return self._signal

    def signals(self):
        return self._signal

    def len(self):
        return self._signal.len()

    def set_bin(self, v):
        self._signal.set_bin(v)

    def bin(self):
        return self._signal.bin()

    def decl(self):
        return self._signal.decl()

class Component(object):
    def __init__(self, name):
        self._pkgs  = []
        self._out    = []
        self._in     = []
        self._cases = {}
        self._others = None
        self._tpl_file = path.join(tpl_dir, "case.tpl")
        self._name = name

    def __setattr__(self, key, value):
        if isinstance(value, Port):
            if "i" in value._dir:
                self._in.append(value)
            if "o" in value._dir:
                self._out.append(value)
            value._set_name(key)
            object.__setattr__(self, key, value._get_signal())
        else:
            object.__setattr__(self, key, value)

    def add_package(self, pkg):
        self._pkgs.append(pkg)

    def input(self):
        return self._in

    def output(self):
        return self._out

    def capture(self):
        select = binjoin_fields(*self._in)
        choice = binjoin_fields(*self._out)
        self._cases[select] = choice

    def in_len(self):
        return get_total_len(*self._in)

    def out_len(self):
        return get_total_len(*self._out)

    def get_default(self):
        if self._others == None:
            total_len = get_total_len(*self._out)
            self._others = std_logic_vector(total_len)()
        return self._others

    def capture_default(self):
        total_len = get_total_len(*self._out)
        self._others = std_logic_vector(total_len)()
        self._others.set_bin(binjoin_fields(*self._out))

    def bin(self):
        re = ""
        for p in self.__input.items():
            re += p.bin()
        for p in self.__output.items():
            re += p.bin()
        return re

    def len(self):
        return len(self._cases)

    def build_file(self):
        out_file = self._name + ".vhd"

        tpl = {}

        pkg_t = "use work.%s.all;\n"
        pkgs = "".join([pkg_t % i for i in self._pkgs])
        tpl["packages"] = pkgs[:-1]

        tpl["name"] = self._name

        # signal_name: direction type_with_length
        port_l = '%s : %s %s;\n'
        ports = ""
        for p in self._in:
            ports += port_l % (p._get_name(), "in", p.decl())
        for p in self._out:
            ports += port_l % (p._get_name(), "out", p.decl())
        tpl["ports"] = ports[:-2]

        k = self.in_len()
        in_t = "std_logic_vector(%d downto 0)" % (k - 1)
        tpl["in_type"] = in_t

        k = self.out_len()
        out_t = "std_logic_vector(%d downto 0)" % (k - 1)
        tpl["out_type"] = out_t

        assign_l = "%s <= %s;\n"

        def traverse(signal, curr_name, l, idx, some, start=False):
            for i in signal.signals():
                    curr = curr_name
                    if(isinstance(signal, StdLogicBase) or
                        isinstance(signal, StdLogicVectorBase)):

                        idx_b = idx - signal.len()
                        if(signal.len() > 1):
                            body = "%s(%d downto %d)" % (l, idx - 1, idx_b)
                        else:
                            body = "%s(%d)" % (l, idx - 1)
                        aux = ".".join([curr_name, signal._get_name()])
                        some.append([aux, body])
                        idx = idx_b
                        return idx

                    curr = ".".join([curr, signal._get_name()])
                    if i != signal:
                        idx = traverse(i, curr, l, idx, some);
            return idx

        some = []
        idx = self.in_len()
        for s in self._in:
            idx = traverse(s._get_signal(), "", "addr", idx, some, True)

        assign = ""
        for line in some:
            assign += assign_l % (line[1], line[0][1:])
        tpl["in_assign"] = assign[:-1]

        some = []
        idx = self.out_len()
        for s in self._out:
            idx = traverse(s._get_signal(), "", "data", idx, some, True)

        assign = ""
        for line in some:
            assign += assign_l % (line[0][1:], line[1])
        tpl["out_assign"] = assign[:-1]

        case_t = 'when "%s" => data <= "%s";\n'
        cases = ""
        for c in sorted(self._cases.keys()):
            cases += case_t % (c, self._cases[c])
        tpl["cases"] = cases[:-1]

        others_t = 'when others => data <= "%s";'
        others = others_t % (self.get_default().bin())
        tpl["others"] = others


        with open(self._tpl_file, 'r') as f:
            src = Template(f.read())
        vhdl_code  = src.safe_substitute(tpl)
        with open(out_file, 'w') as f:
            f.write(vhdl_code)
