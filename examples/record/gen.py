#!/usr/bin/python3

import sys, os
mylibdir = os.path.expanduser('~/src/python-libs')
sys.path.append(mylibdir)

from pyvhtb.vhtypes import *
from pyvhtb.case import *

from random import randint

N = 10
RST_TICKS = 1

ADDR_REF = 5000
TIME_REF = 2000

TDC_ADDR_RANGE = 1000
TDC_TIME_RANGE = 500
ROI_TUBE_RANGE = 700
ROI_BX_RANGE = 300

TDC_WIDTH_MAX = 255
TDC_WIDTH_MIN = 0
TDC_CTIME_MAX = 2047
TDC_CTIME_MIN = 0
TDC_FTIME_MAX = 31
TDC_FTIME_MIN = 0

# -- data types ----------------------------------------------------

nt = NewTypes() # new types must be registered

tdc_hit_s = RecordStruct()
tdc_hit_s.mezz   = std_logic_vector( 8)
tdc_hit_s.chan   = std_logic_vector( 5)
tdc_hit_s.width  = std_logic_vector( 8)
tdc_hit_s.c_time = std_logic_vector(12)
tdc_hit_s.f_time = std_logic_vector( 5)

nt.tdc_hit_t = record(tdc_hit_s)

roi_local_s = RecordStruct()
roi_local_s.tube_num_lo = std_logic_vector(13)
roi_local_s.tube_num_hi = std_logic_vector(13)
roi_local_s.bx_lo  = std_logic_vector(12)
roi_local_s.bx_hi  = std_logic_vector(12)

nt.roi_local_t = record(roi_local_s)

hit_s = RecordStruct()
hit_s.tdc = nt.tdc_hit_t
hit_s.dav = std_logic()
hit_s.roi = nt.roi_local_t

nt.hit_t = record(hit_s)

nt.step_t = std_logic_vector(16)
nt.bit_t = std_logic()

# -- blocks declaration --------------------------------------------

# control block
c = Component("ctrl")
c.step      = Port("i", nt.step_t)
c.next_step = Port("o", nt.step_t)
c.rst       = Port("o", nt.bit_t)

# data block generates data for dut
g = Component("gen")
g.step = Port("i", nt.step_t)
g.hit  = Port("o", nt.hit_t)
g.add_package("tdc_pkg")

# dut ref block
r = Component("ref")
r.step  = Port("i", nt.step_t)
r.hit_i = Port("i", nt.hit_t)
r.hit_o = Port("o", nt.hit_t)
r.add_package("tdc_pkg")

# chosen hits must acknowledge and data must be correct
e = Component("check")
e.step = Port("i", nt.step_t)
e.hit  = Port("i", nt.hit_t)
e.err  = Port("o", nt.bit_t)
e.add_package("tdc_pkg")

# -- helpers ----------------------------------------------------

tdc_addr_max = ADDR_REF + TDC_ADDR_RANGE
tdc_addr_min = ADDR_REF - TDC_ADDR_RANGE
tdc_time_max = TIME_REF + TDC_TIME_RANGE
tdc_time_min = TIME_REF - TDC_TIME_RANGE

roi_tube_max = ADDR_REF + ROI_TUBE_RANGE
roi_tube_min = ADDR_REF - ROI_TUBE_RANGE
roi_bx_max   = TIME_REF + ROI_BX_RANGE
roi_bx_min   = TIME_REF - ROI_BX_RANGE

def bin_to_int(x):
    return int(x, 2)

# -- blocks behavior --------------------------------------------

def update_ctrl_signals(n):

    c.step.set_bin(format(n, '0%db' % c.step.len()))

    rst = '1' if n < RST_TICKS else '0'
    c.rst.set_bin(rst)

    nstep = n + 1 if n < N else N
    nstep_fmt = '0%db' % c.next_step.len()
    c.next_step.set_bin(format(nstep, nstep_fmt))

def update_gen_signals(n):

    g.step.set_bin(format(n, '0%db' % c.step.len()))

    tdc_max = int('1' * g.hit.tdc.len(), 2)
    tdc_min = int('0' * g.hit.tdc.len(), 2)
    fmt = "0%db" % g.hit.tdc.len()
    data = format(randint(tdc_min, tdc_max), fmt)
    g.hit.tdc.set_bin(data)

    tdc_addr = Composite(g.hit.tdc.mezz, g.hit.tdc.chan)
    fmt = "0%db" % tdc_addr.len()
    data = format(randint(tdc_addr_min, tdc_addr_max), fmt)
    tdc_addr.set_bin(data)


    data = '1'
    g.hit.dav.set_bin(data)

def update_ref_signals(n):

    if n >= RST_TICKS:

        r.step.set_bin(format(n, '0%db' % c.step.len()))

        r.hit_i.set_bin(g.hit.bin())

        r.hit_o.set_bin(r.hit_i.bin())

        addr_bin = binjoin_fields(r.hit_i.tdc.mezz, r.hit_i.tdc.chan)
        addr = bin_to_int(addr_bin)

        c_time = bin_to_int(r.hit_i.tdc.c_time.bin())

        tube_num_hi = bin_to_int(r.hit_i.roi.tube_num_hi.bin())
        tube_num_lo = bin_to_int(r.hit_i.roi.tube_num_lo.bin())
        bx_hi = bin_to_int(r.hit_i.roi.bx_hi.bin())
        bx_lo = bin_to_int(r.hit_i.roi.bx_lo.bin())

        if(r.hit_i.dav.bin() == '1'
           and tube_num_hi >= addr and addr >= tube_num_lo
           and bx_hi >= c_time and c_time >= bx_lo):
            r.hit_o.dav.set_bin('1')
        else:
            r.hit_o.dav.set_bin('0')

def update_chk_signals(n):

    if n >= RST_TICKS:
        e.step.set_bin(format(n, '0%db' % c.step.len()))
        e.hit.set_bin(g.hit.bin())
        e.err.set_bin('0')


# -- signals generation -----------------------------------------

# roi is static for now

fmt = "0%db" % g.hit.roi.tube_num_lo.len()
g.hit.roi.tube_num_lo.set_bin(format(roi_tube_min, fmt))
g.hit.roi.tube_num_hi.set_bin(format(roi_tube_max, fmt))

fmt = "0%db" % g.hit.roi.bx_lo.len()
g.hit.roi.bx_lo.set_bin(format(roi_bx_min, fmt))
g.hit.roi.bx_hi.set_bin(format(roi_bx_max, fmt))

c.rst.set_bin('1')
for i in range(N):

    update_ctrl_signals(i)
    update_gen_signals(i)
    update_ref_signals(i)
    update_chk_signals(i)

    c.capture()
    g.capture()
    r.capture()
    e.capture()

c.build_file()
g.build_file()
r.build_file()
e.build_file()

print("ctrl: %d." % c.len())
print("gen: %d." % g.len())
print("ref: %d." % r.len())
print("err: %d." % e.len())
