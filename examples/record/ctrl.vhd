library ieee;
use ieee.std_logic_1164.all;



entity ctrl is
  port(
step : in std_logic_vector(15 downto 0);
next_step : out std_logic_vector(15 downto 0);
rst : out std_logic
  );
end entity ctrl;

architecture behavioral of ctrl is

signal addr : std_logic_vector(15 downto 0);
signal data : std_logic_vector(16 downto 0);

begin

addr(15 downto 0) <= step;

next_step <= data(16 downto 1);
rst <= data(0);

process(addr)
begin
  case addr is
when "0000000000000000" => data <= "00000000000000011";
when "0000000000000001" => data <= "00000000000000100";
when "0000000000000010" => data <= "00000000000000110";
when "0000000000000011" => data <= "00000000000001000";
when "0000000000000100" => data <= "00000000000001010";
when "0000000000000101" => data <= "00000000000001100";
when "0000000000000110" => data <= "00000000000001110";
when "0000000000000111" => data <= "00000000000010000";
when "0000000000001000" => data <= "00000000000010010";
when "0000000000001001" => data <= "00000000000010100";
when others => data <= "00000000000000000";
  end case;
end process;

end architecture behavioral;
