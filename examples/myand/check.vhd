library ieee;
use ieee.std_logic_1164.all;



entity check is
  port(
step : in std_logic_vector(15 downto 0);
y : in std_logic;
err : out std_logic
  );
end entity check;

architecture behavioral of check is

signal addr : std_logic_vector(16 downto 0);
signal data : std_logic_vector(0 downto 0);

begin

addr(16 downto 1) <= step;
addr(0) <= y;

err <= data(0);

process(addr)
begin
  case addr is
when "00000000000000000" => data <= "0";
when "00000000000000010" => data <= "0";
when "00000000000000100" => data <= "0";
when "00000000000000110" => data <= "0";
when "00000000000001001" => data <= "0";
when "00000000000001010" => data <= "0";
when "00000000000001100" => data <= "0";
when "00000000000001110" => data <= "0";
when "00000000000010000" => data <= "0";
when "00000000000010010" => data <= "0";
when "00000000000010100" => data <= "0";
when "00000000000010111" => data <= "0";
when "00000000000011001" => data <= "0";
when "00000000000011010" => data <= "0";
when "00000000000011100" => data <= "0";
when "00000000000011110" => data <= "0";
when "00000000000100000" => data <= "0";
when "00000000000100011" => data <= "0";
when "00000000000100100" => data <= "0";
when "00000000000100111" => data <= "0";
when "00000000000101001" => data <= "0";
when "00000000000101011" => data <= "0";
when "00000000000101101" => data <= "0";
when "00000000000101110" => data <= "0";
when "00000000000110000" => data <= "0";
when "00000000000110010" => data <= "0";
when "00000000000110100" => data <= "0";
when "00000000000110110" => data <= "0";
when "00000000000111000" => data <= "0";
when "00000000000111011" => data <= "0";
when "00000000000111100" => data <= "0";
when "00000000000111111" => data <= "0";
when "00000000001000000" => data <= "0";
when "00000000001000011" => data <= "0";
when "00000000001000101" => data <= "0";
when "00000000001000110" => data <= "0";
when "00000000001001000" => data <= "0";
when "00000000001001010" => data <= "0";
when "00000000001001100" => data <= "0";
when "00000000001001110" => data <= "0";
when "00000000001010001" => data <= "0";
when "00000000001010011" => data <= "0";
when "00000000001010100" => data <= "0";
when "00000000001010110" => data <= "0";
when "00000000001011000" => data <= "0";
when "00000000001011010" => data <= "0";
when "00000000001011100" => data <= "0";
when "00000000001011110" => data <= "0";
when "00000000001100001" => data <= "0";
when "00000000001100010" => data <= "0";
when others => data <= "0";
  end case;
end process;

end architecture behavioral;
