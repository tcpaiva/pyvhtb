library ieee;
use ieee.std_logic_1164.all;

${packages}

entity ${name} is
  port(
${ports}
  );
end entity ${name};

architecture behavioral of ${name} is

signal addr : ${in_type};
signal data : ${out_type};

begin

${in_assign}

${out_assign}

process(addr)
begin
  case addr is
${cases}
${others}
  end case;
end process;

end architecture behavioral;
