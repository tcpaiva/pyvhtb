#!/usr/bin/python3

import sys
import os

mylibdir = os.path.expanduser('~/src/python-libs')
sys.path.append(mylibdir)

from pyvhtb.vhtypes import *
from pyvhtb.case import *

from random import randint

N = 50
RST_TICKS = 1

## new data types ------------------------------------------

nt = NewTypes() # they must be registered

nt.step_t = std_logic_vector(16)
nt.bit_t = std_logic()

## blocks --------------------------------------------------

# control block
c = Component("ctrl")
c.step      = Port("i", nt.step_t)
c.next_step = Port("o", nt.step_t)
c.rst       = Port("o", nt.bit_t)

# data generator
g = Component("data_gen")
g.step = Port("i", nt.step_t)
g.a = Port("o", nt.bit_t)
g.b = Port("o", nt.bit_t)

# dut
r = Component("dut_ref")
r.a = Port("i", nt.bit_t)
r.b = Port("i", nt.bit_t)
r.y = Port("o", nt.bit_t)

# generated data must acknowledge
e = Component("check")
e.step = Port("i", nt.step_t)
e.y    = Port("i", nt.bit_t)
e.err  = Port("o", nt.bit_t)


## helpers -------------------------------------------------

nstep_fmt = step_fmt = '0%db' % c.step.len()

## blocks behavior -----------------------------------------

def update_ctrl_signals(n):

    c.step.set_bin(format(n, step_fmt))

    rst = '1' if n < RST_TICKS else '0'
    c.rst.set_bin(rst)

    step = int(c.step.bin(), 2)
    nstep = n + 1 if n < N - 1 else step;
    c.next_step.set_bin(format(nstep, nstep_fmt))

def update_ref_signals(n):

    r.a.set_bin(g.a.bin())
    r.b.set_bin(g.b.bin())

    a = int(g.a.bin())
    b = int(g.b.bin())
    r.y.set_bin(str(a and b))

def update_gen_signals(n):

    g.step.set_bin(format(n, step_fmt))

    g.a.set_bin(str(randint(0, 1)))
    g.b.set_bin(str(randint(0, 1)))

def update_check_signals(n):

    e.step.set_bin(format(n, step_fmt))
    e.y.set_bin(r.y.bin())
    e.err.set_bin('0')

## run -----------------------------------------------------

# error is high for any other situation not specified here.
e.y.set_bin('1')
e.capture_default()


for i in range(N):

    update_ctrl_signals(i)
    update_gen_signals(i)
    update_ref_signals(i)
    update_check_signals(i)

    c.capture()
    g.capture()
    r.capture()
    e.capture()

# generate vhdl files in the end
c.build_file()
g.build_file()
e.build_file()

# instead of the ref_dut, the real dut will be included in testbench
# ref_dut.vhd generated here only for curiosity
r.build_file()


# quick check for produced data
print("ctrl: %d." % c.len())
print("gen: %d." % g.len())
print("ref: %d." % r.len())
print("err: %d." % e.len())
