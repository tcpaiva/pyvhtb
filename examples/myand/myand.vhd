library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity myand is
    Port ( 
        A : in STD_LOGIC;
        B : in STD_LOGIC;
        Y : out STD_LOGIC
    );
end myand;

architecture Behavioral of myand is

begin
Y <= A and B;

end Behavioral;
