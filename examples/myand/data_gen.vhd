library ieee;
use ieee.std_logic_1164.all;



entity data_gen is
  port(
step : in std_logic_vector(15 downto 0);
a : out std_logic;
b : out std_logic
  );
end entity data_gen;

architecture behavioral of data_gen is

signal addr : std_logic_vector(15 downto 0);
signal data : std_logic_vector(1 downto 0);

begin

addr(15 downto 0) <= step;

a <= data(1);
b <= data(0);

process(addr)
begin
  case addr is
when "0000000000000000" => data <= "10";
when "0000000000000001" => data <= "10";
when "0000000000000010" => data <= "00";
when "0000000000000011" => data <= "10";
when "0000000000000100" => data <= "11";
when "0000000000000101" => data <= "01";
when "0000000000000110" => data <= "01";
when "0000000000000111" => data <= "10";
when "0000000000001000" => data <= "01";
when "0000000000001001" => data <= "10";
when "0000000000001010" => data <= "10";
when "0000000000001011" => data <= "11";
when "0000000000001100" => data <= "11";
when "0000000000001101" => data <= "01";
when "0000000000001110" => data <= "00";
when "0000000000001111" => data <= "00";
when "0000000000010000" => data <= "10";
when "0000000000010001" => data <= "11";
when "0000000000010010" => data <= "00";
when "0000000000010011" => data <= "11";
when "0000000000010100" => data <= "11";
when "0000000000010101" => data <= "11";
when "0000000000010110" => data <= "11";
when "0000000000010111" => data <= "01";
when "0000000000011000" => data <= "00";
when "0000000000011001" => data <= "00";
when "0000000000011010" => data <= "01";
when "0000000000011011" => data <= "01";
when "0000000000011100" => data <= "00";
when "0000000000011101" => data <= "11";
when "0000000000011110" => data <= "01";
when "0000000000011111" => data <= "11";
when "0000000000100000" => data <= "00";
when "0000000000100001" => data <= "11";
when "0000000000100010" => data <= "11";
when "0000000000100011" => data <= "00";
when "0000000000100100" => data <= "01";
when "0000000000100101" => data <= "00";
when "0000000000100110" => data <= "01";
when "0000000000100111" => data <= "00";
when "0000000000101000" => data <= "11";
when "0000000000101001" => data <= "11";
when "0000000000101010" => data <= "01";
when "0000000000101011" => data <= "01";
when "0000000000101100" => data <= "01";
when "0000000000101101" => data <= "10";
when "0000000000101110" => data <= "00";
when "0000000000101111" => data <= "10";
when "0000000000110000" => data <= "11";
when "0000000000110001" => data <= "01";
when others => data <= "00";
  end case;
end process;

end architecture behavioral;
