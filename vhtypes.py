from collections import OrderedDict

def compl2(v, l):
    if v < 0:
        return v + (1 << l)
    return v

VALID = ['U', 'X', '0', '1', 'Z', 'W', 'L', 'H', '-']

def is_valid(v):
    return all(i in VALID for i in v)

def is_invalid(v):
    return not isinstance(v, str) or any(i not in VALID for i in v)


class StdLogicBase(object):

    def __init__(self):
        self._name = None
        self._v = '0'

    @classmethod
    def len(self):
        return 1

    @classmethod
    def decl(self):
        return "std_logic"

    def bin(self):
        return self._v[-1]

    def set_bin(self, v):
        self._v = v[-1]

    def set_z(self):
        self._v = 'Z'

    def items(self):
        return []

    def signals(self):
        #print(self)
        yield self

    def _set_name(self, name):
        self._name = name

    def _get_name(self):
        return self._name

    def set_int(self, v):
        b = format(v, '0%db' % self.len())
        self.set_bin(b)

    def int(self):
        return int(self.bin(), 2)


def std_logic():

    class StdLogic(StdLogicBase):
        pass

    return StdLogic

class StdLogicVectorBase(object):

    @classmethod
    def decl(self):
        return "std_logic_vector(%d downto 0)" % (self.len() - 1)

    def bin(self):
        return "".join(s.bin() for s in self._signals)

    def set_bin(self, v):
        if len(v) != self.len():
            raise ValueError('Wrong length')
        for i in range(self.len()):
            self._signals[i].set_bin(v[i])

    def set_z(self):
        for i in range(self.len()):
            self._signals[i].set_z()


    def items(self):
        return self._signals

    def __str__(self):
        return "".join(str(i) for i in self.items())

    def _set_name(self, name):
        self._name = name

    def _get_name(self):
        return self._name

    def signals(self):
        for s in self._signals:
            yield s

    def set_int(self, v):
        l = self.len()
        b = format(v, '0%db' % l)[-l:]
        self.set_bin(b)

    def int(self):
        return int(self.bin(), 2)


def std_logic_vector(length):
    l = length

    class StdLogicVector(StdLogicVectorBase):

        @classmethod
        def len(self):
            return length

        def __init__(self):
            self._l = l
            self._signals = []
            self._name = None
            for i in range(self.len()):
                self._signals.append(std_logic()())

    return StdLogicVector


class ArrayBase(object):

    def bin(self):
        return "".join(s.bin() for s in self.signals)

    def set_bin(self, v):
        cnt = 0
        for i in range(1, self.len() + 1):
            l = self.signals[-i].len()
            if cnt > 0:
                a = v[-(cnt + l) : -cnt]
            else:
                a = v[-(cnt + l) :]
            self.signals[-i].set_bin(a)
            cnt += l

    def set_z(self):
        v = 'Z' * self.len()
        self.set_bin(v)

    def items(self):
        return self.signals

    def set_int(self, v):
        l = self.len()
        b = format(v, '0%db' % l)[-l:]
        self.set_bin(b)

    def int(self):
        return int(self.bin(), 2)


def array(array_type, length):

    class Array(ArrayBase):

        @classmethod
        def len(self):
            return length

        def __init__(self):
            self.signals = []
            self.array_type = array_type
            for i in range(self.len()):
                self.signals.append(array_type())

        @classmethod
        def decl(self):
            return "%s(%d downto 0)" % (array_type.decl, self.len() - 1)

    return Array


class RecordStruct(object):
    def __init__(self):
        object.__setattr__(self, "_items", OrderedDict())

    def __setattr__(self, key, value):
        self._items[key] = value

    def len(self):
        a = self._items.values()
        return sum([i.len() for i in a])

    def items(self):
        return self._items.items()

class RecordBase(object):

    @classmethod
    def decl(self):
        return "%s" % self._name

    def bin(self):
        return "".join(s.bin() for s in self._signals)

    def set_bin(self, v):
        cnt = 0
        for s in self._signals:
            l = s.len()
            s.set_bin(v[cnt : cnt + l])
            cnt += l

    def set_z(self):
        v = 'Z' * self.len()
        self.set_bin(v)

    def items(self):
        return self._signals
        # return [[i, i.items()] for i in self._signals if i != None]

    def _set_name(self, name):
        self._name = name

    def _get_name(self):
        return self._name

    def signals(self):
        for s in self._signals:
            #print(s)
            yield s

    def set_int(self, v):
        l = self.len()
        b = format(v, '0%db' % l)[-l:]
        self.set_bin(b)

    def int(self):
        return int(self.bin(), 2)


    #def __repr__(self):
    #    return str(list(self.items()))


def record(record_struct):

    length = record_struct.len()

    class Record(RecordBase):

        @classmethod
        def descr(self):
            return record_struct

        @classmethod
        def len(self):
            return length

        def __init__(self):
            self._basedon = RecordBase
            self._signals = []
            self._name = None
            for name, signal in record_struct.items():
                s = signal()
                s._set_name(name)
                setattr(self, name, s)
                self._signals.append(s)

        #def __repr__(self):
        #    return str(list(self.items()))

    return Record


def new_type(b):
   return type('Custom', b.__bases__, dict(b.__dict__))


class NewTypes(object):

    def __setattr__(self, key, value):
        value._name = key
        object.__setattr__(self, key, value)


class Composite(object):

    def __init__(self, *args):
        self._signals = []
        for arg in args:
            self._signals.append(arg)

    def set_bin(self, v):
        i = 0
        for s in self._signals:
            top = i + s.len()
            s.set_bin(v[i:top])
            i = top

    def bin(self):
        aux = [b.bin() for b in self._signals]
        return "".join(aux)

    def len(self):
        length = 0
        for signal in self._signals:
            length += signal.len()
        return length

    def int(self):
        aux = [b.bin() for b in self._signals]
        return int(aux, 2)

    def set_int(self, v):
        b = format(v, '0%db' % self.len())
        self.set_bin(b)
