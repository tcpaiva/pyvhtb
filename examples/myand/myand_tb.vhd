library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

entity myand_tb is
--  empty
end myand_tb;

architecture Behavioral of myand_tb is
    
    -- myand
    signal A : STD_LOGIC;
    signal B : STD_LOGIC;
    signal Y : STD_LOGIC;

    -- ctrl
    signal step : std_logic_vector(15 downto 0);
    signal next_step : std_logic_vector(step'range);
    signal rst : std_logic;

    -- check
    signal err_int : std_logic;

    -- tb    
    signal clk : std_logic := '0';
    signal err : std_logic;

begin

    clk <= not clk after 5ns;
    
    process(clk)
    begin
        if falling_edge(clk) then
            step <= next_step;
        end if;
        
        if rising_edge(clk) then
            err <= err_int;
        end if;
    end process;
    

    -- replaces the "dut_ref" module
    myand : entity work.myand port map(
        A, -- : in STD_LOGIC;
        B, -- : in STD_LOGIC;
        Y -- : out STD_LOGIC
    );

    ctrl : entity work.ctrl port map (
        step, -- : in std_logic_vector(15 downto 0);
        next_step, -- : out std_logic_vector(15 downto 0);
        rst -- : out std_logic
    );

    data_gen : entity work.data_gen port map(
        step, -- : in std_logic_vector(3 downto 0);
        A, -- : out std_logic;
        B -- : out std_logic
    );

    -- inputs the "real" dut output
    check : entity work.check port map(
        step, -- : in std_logic_vector(3 downto 0);
        Y, -- : out std_logic;
        err_int -- : out std_logic
    );

end Behavioral;





