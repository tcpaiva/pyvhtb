library ieee;
use ieee.std_logic_1164.all;



entity dut_ref is
  port(
a : in std_logic;
b : in std_logic;
y : out std_logic
  );
end entity dut_ref;

architecture behavioral of dut_ref is

signal addr : std_logic_vector(1 downto 0);
signal data : std_logic_vector(0 downto 0);

begin

addr(1) <= a;
addr(0) <= b;

y <= data(0);

process(addr)
begin
  case addr is
when "00" => data <= "0";
when "01" => data <= "0";
when "10" => data <= "0";
when "11" => data <= "1";
when others => data <= "0";
  end case;
end process;

end architecture behavioral;
