Helper tool to generate testbenchs. With `pyvhtb` it is possible to only
describe the expected results with no need for a real HDL description. The types
used by this tool try to mimic the behavior of VHDL types.

There are many ways to use this tool and an "AND" module is in the examples
folder with its related testbench. There, a whole set of files were
prepared not only to generate the data for the DUT, but also to check
if the results are as expected.  Must be highlighted that this tool does not
generate the testbench itself for now, but its creation using the generated
modules is trivial. Another way to use this tool is to just generate
the simulation data, without worries about automatic verification.

Another point is that this tool generates VHDL code, therefore it can
be used with development environment from any manufacturer (Xilinx,
Altera, etc).

To install, just clone this repository and add the parent folder to the Python
path, as shown in the mentioned "AND" example.

Tool still under development, use with care. It still lacks errors
checking related to usual mistakes made by users.

Some examples of use are found below:

``` python
# gen.py

# new types must be registered
nt = NewTypes()

# creating the structure of the record
my_record_r = RecordStruct()
my_record_r.a = std_logic()
my_record_r.b = std_logic_vector(4)
my_record_r.c = array(std_logic_vector(16), 4)

# generated the record type with above structure
nt.my_record_t = record(tdc_hit_r)

# new type based on the std_logic
nt.bit_t = std_logic()

# another record structure, using custom types, including nested records
other_record_r = RecordStruct()
other_record_r.d = nt.my_record_t
other_record_r.e = nt.bit_t
nt.other_record_t = record(other_record_r)

# number of bits in other_record_t
nt.other_record_t.len()
```

More real example below

``` python
# Reference block for DUT. It will generate a vhdl file named "dut_ref" in the
# same folder of the script, with the behavior of the block being emulated by a
# ROM memory. Please see examples folder.
r = Component("dut_ref")
r.a = Port("i", nt.bit_t) # input port
r.b = Port("i", nt.bit_t) # input port
r.y = Port("o", nt.bit_t) # output port

r.a.set_bin('1')
r.b.set_bin('1')
r.y.set_bin('1')
r.capture() # include this situation in the "ROM"

# all other situations regarding inputs will result as '0' on the output
r.set_default('0')

# build the VHDL file related to the "dut_ref" component
r.build_file()
```
